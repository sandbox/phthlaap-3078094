#INTRODUCTION

This is my first exercise for automated test in Drupal development.

#Requirements

```
The form to test have some fields below:

- title
		type: textfield
		label: title, placeholder: Title, Maxlength: 255
		Validate: only contain alphabet, not contain number, not null
- body
 		type: textarea, long plain text
 		label: Please input your content
 		Validate: not allow HTML tag, not null
- tags
		type: autocomplete field, taxonomy term reference
		label: Select tags
		Validate: max 5 tags

- submit button


When user input all mandatory data then submit, we will create an article with that data.```
