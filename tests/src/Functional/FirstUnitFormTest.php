<?php

namespace Drupal\Tests\first_unit\Functional;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Url;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\field\Traits\EntityReferenceTestTrait;
use Drupal\Tests\first_unit\Traits\FirstUnitFormTestTrait;

/**
 * Class FirstUnitFormTest.
 *
 * @package Drupal\Tests\first_unit\Functional
 */
class FirstUnitFormTest extends BrowserTestBase {

  use FirstUnitFormTestTrait;
  use EntityReferenceTestTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['taxonomy', 'filter', 'first_unit'];

  protected $formRoute;

  /**
   * Tags to test.
   *
   * @var array
   */
  protected $tags;

  /**
   * Field tags name.
   *
   * @var string
   */
  protected $fieldTagsName;

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    $this->formRoute = 'first_unit.first_unit_form';
    $vocabulary = Vocabulary::create([
      'name' => 'tags',
      'description' => $this->randomMachineName(),
      'vid' => 'tags',
      'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
      'weight' => mt_rand(0, 10),
    ]);
    $vocabulary->save();
    $this->tags[] = $this->createTerm($vocabulary, ['name' => 'test tag 1']);
    $this->tags[] = $this->createTerm($vocabulary, ['name' => 'test tag 2']);
    $this->tags[] = $this->createTerm($vocabulary, ['name' => 'test tag 3']);
    $this->tags[] = $this->createTerm($vocabulary, ['name' => 'test tag 4']);
    $this->tags[] = $this->createTerm($vocabulary, ['name' => 'test tag 5']);
    $this->tags[] = $this->createTerm($vocabulary, ['name' => 'test tag 6']);

    $vocabulary2 = Vocabulary::create([
      'name' => $this->randomMachineName(),
      'description' => $this->randomMachineName(),
      'vid' => mb_strtolower($this->randomMachineName()),
      'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
      'weight' => mt_rand(0, 10),
    ]);
    $this->tags[] = $this->createTerm($vocabulary2, ['name' => 'test tag 7']);
    $this->tags[] = $this->createTerm($vocabulary2, ['name' => 'test tag 8']);

    $this->drupalCreateContentType(['type' => 'article']);

    $this->fieldTagsName = 'field_tags';
    $handler_settings = [
      'target_bundles' => ['tags'],
    ];
    $this->createEntityReferenceField('node', 'article', $this->fieldTagsName, 'Tags', 'taxonomy_term', 'default', $handler_settings, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository */
    $entity_display_repository = \Drupal::service('entity_display.repository');
    $entity_display_repository->getFormDisplay('node', 'article')
      ->setComponent($this->fieldTagsName, [
        'type' => 'entity_reference_autocomplete_tags',
        'weight' => 10,
      ])
      ->save();
  }

  /**
   * Test the form appear when access the link.
   */
  public function testTheFormAppear() {
    $this->drupalGet(Url::fromRoute($this->formRoute));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'input[name="title"]');
    $this->assertSession()->elementExists('css', 'textarea[name="body"]');
    $this->assertSession()->elementExists('css', 'input[name="tags"]');
    $this->assertSession()->elementExists('css', 'input[type="submit"]');
  }

  /**
   * Test title field attributes (placeholder).
   *
   * @depends testTheFormAppear
   */
  public function testTitleFieldAttribute() {
    $this->drupalGet(Url::fromRoute($this->formRoute));
    $page = $this->getSession()->getPage();
    $placeholder = $page->find('css', 'input[name="title"]')
      ->getAttribute('placeholder');
    $this->assertEquals('Title', $placeholder);
  }

  /**
   * Test validate title field.
   *
   * @depends testTheFormAppear
   */
  public function testTitleFieldNotNull() {
    $this->drupalGet(Url::fromRoute($this->formRoute));
    $this->getSession()->getPage()->pressButton('edit-submit');
    $this->assertSession()->pageTextContains('Title cannot be null');
  }

  /**
   * Test title field max length.
   *
   * @depends testTheFormAppear
   */
  public function testTitleFieldMaxLength() {
    $this->drupalGet(Url::fromRoute($this->formRoute));
    $this->getSession()
      ->getPage()
      ->fillField('edit-title', $this->randomString(500));
    $this->getSession()->getPage()->pressButton('edit-submit');
    $this->assertSession()
      ->pageTextContains('Title cannot be longer than 255 characters but is currently 500 characters long.');

  }

  /**
   * Test title field only contain alphabet.
   *
   * @depends testTheFormAppear
   */
  public function testTitleFieldOnlyContainAlphabet() {
    $this->drupalGet(Url::fromRoute($this->formRoute));
    $this->getSession()
      ->getPage()
      ->fillField('edit-title', 'This title cannot contain 123');
    $this->getSession()->getPage()->pressButton('edit-submit');
    $this->assertSession()
      ->pageTextContains('Title cannot contain number or special characters.');

    $this->getSession()
      ->getPage()
      ->fillField('edit-title', 'This title cannot contain @');
    $this->getSession()->getPage()->pressButton('edit-submit');
    $this->assertSession()
      ->pageTextContains('Title cannot contain number or special characters.');
  }

  /**
   * Test body field not null.
   *
   * @depends testTheFormAppear
   */
  public function testBodyFieldNotNull() {
    $this->drupalGet(Url::fromRoute($this->formRoute));
    $this->getSession()->getPage()->pressButton('edit-submit');
    $this->assertSession()->pageTextContains('Body cannot be null.');
  }

  /**
   * Test body field cannot contain HTML.
   *
   * @depends testTheFormAppear
   */
  public function testBodyFieldCannotContainHtml() {
    $this->drupalGet(Url::fromRoute($this->formRoute));
    $html = '<a>link</a><script>alert(2)</script>';

    $this->getSession()->getPage()->fillField('edit-body', $html);
    $this->getSession()->getPage()->pressButton('edit-submit');
    $this->assertSession()->pageTextContains('Body cannot contain HTML tag.');

    $this->getSession()
      ->getPage()
      ->fillField('edit-body', 'this text not contain any tag');
    $this->getSession()->getPage()->pressButton('edit-submit');
    $this->assertSession()
      ->pageTextNotContains('Body cannot contain HTML tag.');
  }

  /**
   * Test max 5 tags selected.
   *
   * @depends testTheFormAppear
   */
  public function testValidateMax5TagsSelected() {
    $this->drupalGet(Url::fromRoute($this->formRoute));
    $page = $this->getSession()->getPage();

    $tags = $this->buildTagNameForInput($this->tags, 6);

    $page->fillField('edit-tags', implode(', ', $tags));
    $page->pressButton('edit-submit');
    $this->assertSession()
      ->pageTextContains('You cannot select more than 5 tags.');
  }

  /**
   * Test number valid of tags selected.
   *
   * @depends testTheFormAppear
   */
  public function testValidNumberOfTagsSelected() {
    $this->drupalGet(Url::fromRoute($this->formRoute));
    $page = $this->getSession()->getPage();
    $tags = $this->buildTagNameForInput($this->tags, 2);
    $page->fillField('edit-tags', implode(', ', $tags));
    $page->pressButton('edit-submit');
    $this->assertSession()
      ->pageTextNotContains('You cannot select more than 5 tags.');
  }

  /**
   * Test artice entity created when valid input.
   *
   * @depends testTheFormAppear
   */
  public function testArticleCreatedWhenValidInput() {
    $tags = $this->buildTagNameForInput($this->tags, 2);
    $this->drupalGet(Url::fromRoute($this->formRoute));

    $title = 'This is a valid title A';
    $body = 'this is a valid body, not contain html tag.';

    $page = $this->getSession()->getPage();
    $page->fillField('edit-title', $title);
    $page->fillField('edit-body', $body);
    $page->fillField('edit-tags', implode(', ', $tags));
    $page->pressButton('edit-submit');

    $this->assertSession()->pageTextContains('Your article has been created.');
    $articles = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadByProperties([
        'type' => 'article',
      ]);
    $this->assertEquals(1, count($articles), 'Expected 1 article create after valid submit, found: ' . count($articles));
    $articles = array_values($articles);
    /** @var \Drupal\node\NodeInterface $article */
    $article = $articles[0];
    $this->assertEquals($title, $article->getTitle(), 'Expected title is "' . $title . '" but result is "' . $article->getTitle() . '"');
    $this->assertEquals(TRUE, $article->hasField('body'), 'Expected article have body field.');
    $this->assertEquals($body, $article->get('body')->value, 'Expected body is "' . $body . '" but result is "' . $article->get('body')->value . '"');

    $this->assertTrue($article->hasField('field_tags'), 'Expected tags fields exist in article.');
    $terms = $article->get('field_tags')->referencedEntities();
    $this->assertNotEmpty($terms, 'Expected article have tags value but result is "' . var_export($terms, TRUE) . '"');
    $this->assertInstanceOf('Drupal\taxonomy\Entity\Term', $terms[0]);
  }

}
