<?php

namespace Drupal\Tests\first_unit\Traits;

use Drupal\Core\Language\LanguageInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\VocabularyInterface;

trait FirstUnitFormTestTrait {

  public function createTerm(VocabularyInterface $vocabulary, $values = []) {
    if (empty($values['name'])) {
      $values['name'] = $this->randomMachineName();
    }
    $term = Term::create($values + [
      'description' => [
        'value' => $this->randomMachineName(),
        // Use the fallback text format.
        'format' => filter_fallback_format(),
      ],
      'vid' => $vocabulary->id(),
      'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
    ]);
    $term->save();
    return $term;
  }

  public function buildTagNameForInput($tags_to_build, $number_of_tag_to_fill = 5) {
    $tags = [];
    /** @var \Drupal\taxonomy\TermInterface $tag */
    foreach ($tags_to_build as $tag) {
      $tags[] = $tag->getName() . ' (' . $tag->id() . ')';
      if (count($tags) >= $number_of_tag_to_fill) {
        break;
      }
    }
    return $tags;
  }

}