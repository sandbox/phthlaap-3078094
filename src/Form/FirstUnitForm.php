<?php

namespace Drupal\first_unit\Form;

use Drupal\Component\Transliteration\TransliterationInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Render\RendererInterface;

/**
 * Class FirstUnitForm.
 */
class FirstUnitForm extends FormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Form\FormBuilderInterface definition.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Drupal\Core\Render\RendererInterface definition.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  protected $messenger;

  protected $transliterate;

  /**
   * Constructs a new FirstUnitForm object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FormBuilderInterface $form_builder, RendererInterface $renderer, MessengerInterface $messenger, TransliterationInterface $transliteration) {
    $this->entityTypeManager = $entity_type_manager;
    $this->formBuilder = $form_builder;
    $this->renderer = $renderer;
    $this->messenger = $messenger;
    $this->transliterate = $transliteration;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'), $container->get('form_builder'), $container->get('renderer'), $container->get('messenger'), $container->get('transliteration'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'first_unit_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => 'Title',
      '#maxlength' => 255,
      '#placeholder' => 'Title',
    ];
    $form['body'] = [
      '#type' => 'textarea',
      '#title' => 'Please input your content',
    ];
    $form['tags'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'taxonomy_term',
      '#title' => 'Taxonomy Term',
      '#tags' => TRUE,
      '#selection_settings' => [
        'target_bundles' => ['tags'],
      ],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (empty($values['title'])) {
      $form_state->setErrorByName('title', 'Title cannot be null');
    }
    elseif (!preg_match('/^[a-z A-Z\'\-]+$/', $this->transliterate->transliterate($values['title']), $output)) {
      $form_state->setErrorByName('title', 'Title cannot contain number or special characters.');
    }
    if (empty($values['body'])) {
      $form_state->setErrorByName('body', 'Body cannot be null.');
    }
    elseif (strip_tags($values['body']) != $values['body']) {
      $form_state->setErrorByName('body', 'Body cannot contain HTML tag.');
    }
    if (!empty($values['tags']) && count($values['tags']) > 5) {
      $form_state->setErrorByName('tags', 'You cannot select more than 5 tags.');
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getErrors())) {
      $article = $this->entityTypeManager->getStorage('node')->create([
        'type' => 'article',
        'title' => $form_state->getValue('title'),
        'body' => $form_state->getValue('body'),
        'field_tags' => $form_state->getValue('tags'),
      ]);
      $article->save();
      $this->messenger->addMessage('Your article has been created.');
    }
  }

}
